# Open Science Workshop | ÚMS SCI MUNI, Brno 2024

> Následuje česká verze

This is a repository containing materials shared and/or used during the Open Science Workshop, which happened on the 16th of February 2024 on the premises of the Department of Mathematics and Statistics, SCI MUNI.

It consisted of 5 talks in general. Materials for them should be added here sooner or later.

### Introduction to Open & Shared Science

Introductory talk by Assoc. Prof. Lenka Přibylová about the importance and principles of open and shared science.

Slides can be found in the `Introduction` folder.

### Data & Databases

General introduction to the problem of data and their usage/storage and general handling by dr. Veronika Eclerová.

`#TODO: Slides will be added later`


### Python and the cloud environment Google Colab

Hands-on tutorial to using Python in the Google Colab environment mainly focused on the data science aspect.

`#TODO: Notebook will be added later`

### GIT GOOD or how to develop, version and share code

Quick and dirty tutorial to the problem of versioning code, using `git` to solve this problem and best practices of doing such.

The presentation can be found in a standalone repository: https://gitlab.com/sceptri-university/git-good-workshop

### Open Science with MATLAB

An overarching talk by Humusoft about MATLAB's role in the open science ecosystem mainly focused on various tools provided by MathWorks.

Materials Humusoft provided can be found here: https://content.mathworks.com/viewer/65d305291f061a03054b1f0f

---

# Workshop o sdílené a otevřené vědě

Tento repozitář obsahuje materiály nasdílené či využité během workshopu o otevřené vědě, jenž proběhl 16.2.2024 na půdě Ústavu Matematiky a Statistiky, PřF MUNI.

Workshop se skládá z 5 přednášek. Materiály pro každou z nich budou přidány.

### Úvod o sdílené a otevřené vědě

Úvodní přednáška doc. Lenky Přibylové, Ph.D. o významu a principech otevřené a sdílené vědy.

Slidy lze nalézt ve složce `Introduction`.

### Data a databáze

Obecný úvod do problematiky dat a jejich uložení/použití či obecně zacházení od RNDr. Veroniky Eclerové, Ph.D.

`#TODO: Slidy budou přidány později`

### Python a prostředí Google Colab

Praktický tutoriál Bc. Markéty Trembaczové o použití Pythonu v cloudovém prostředí Google Colab se zaměřením na aspekty týkající se datové vědy.

`#TODO: Notebook bude přidán později`


### GIT GOOD aneb jak vyvíjet, verzovat a sdílet kód

Rychlý a stručný úvod do problematiky verzování kódu a (efektivního) použití `git`u na vyřešení tohoto problému.

Prezentaci lze najít v samostatném repozitáři: https://gitlab.com/sceptri-university/git-good-workshop

### Otevřená věda v prostředí MATLAB

Přehledová přednáška od Humusoftu o roli MATLABu v komunitě otevřené a sdílené vědy s hlavním zaměřením na různé nástroje vyvíjené firmou MathWorks.

Materiály od Humusoftu jsou dostupné zde: https://content.mathworks.com/viewer/65d305291f061a03054b1f0f
